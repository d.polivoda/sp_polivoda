﻿using System.Windows;


namespace Data_Grid_Polivoda
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            Car Opel = new Car();


            Opel.Type = "Sedan";
            Opel.Brand = "Opel";
            Opel.Model = "Kadet";
            Opel.Year = "1991";
            Opel.Price = "2000$";
            Opel.Region = "Nikolaev";

            Database.Items.Add(Opel);

            Car Honda = new Car();


            Honda.Type = "Cuv";
            Honda.Brand = "Honda";
            Honda.Model = "Pilot";
            Honda.Year = "2008";
            Honda.Price = "12000$";
            Honda.Region = "Kyiv";

            Database.Items.Add(Honda);


        }

        public class Car
        {
            public string Type { get; set; }
            public string Brand { get; set; }
            public string Model { get; set; }
            public string Year { get; set; }
            public string Price { get; set; }
            public string Region { get; set; }


        }

        private void AddBN_Click(object sender, RoutedEventArgs e)
        {
            Car NEW = new Car();

            NEW.Type = Type_B.Text;
            NEW.Brand = Brand_B.Text;
            NEW.Model = Model_B.Text;
            NEW.Year = Year_B.Text;
            NEW.Price = Price_B.Text;
            NEW.Region = Region_B.Text;

            Database.Items.Add(NEW);
        }
    }
}
