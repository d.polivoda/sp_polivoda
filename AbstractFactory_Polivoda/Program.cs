﻿using System;
namespace AbstractFactoryDesignPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Product product = null;
            ProductFactory productFactory = null;
            string value = null;
            
            productFactory = ProductFactory.CreateProductFactory("Vegetables");
            Console.WriteLine("Product Factory type : " + productFactory.GetType().Name);
            Console.WriteLine();
        
            product = productFactory.GetProduct("Tomato");
            Console.WriteLine("Product: " + product.GetType().Name);
            value = product.price();
            Console.WriteLine(product.GetType().Name + "Price: " + value);
            Console.WriteLine();
            Console.WriteLine("--------------------------");
         
            productFactory = ProductFactory.CreateProductFactory("Milk");
            Console.WriteLine("Product Factory type : " + productFactory.GetType().Name);
            Console.WriteLine();
          
            product = productFactory.GetProduct("Cheese");
            Console.WriteLine("Product: " + product.GetType().Name);
            value = product.price();
            Console.WriteLine(product.GetType().Name + " Price: " + value);
            Console.WriteLine();
          
            product = productFactory.GetProduct("Milk");
            Console.WriteLine("Product: " + product.GetType().Name);
            value = product.price();
            Console.WriteLine(product.GetType().Name + " Price: " + value);
            Console.Read();
        }
    }
}