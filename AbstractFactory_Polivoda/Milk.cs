﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactoryDesignPattern
{
    public class Milk : Product
    {
        public string price()
        {
            return "30 grn";
        }
    }
}