﻿namespace AbstractFactoryDesignPattern
{
    public class Cheese : Product
    {
        public string price()
        {
            return "150 grn";
        }
    }
}