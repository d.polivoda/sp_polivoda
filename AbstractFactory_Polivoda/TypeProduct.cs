﻿namespace AbstractFactoryDesignPattern
{
    public abstract class ProductFactory
    {
        public abstract Product GetProduct(string ProductType);
        public static ProductFactory CreateProductFactory(string FactoryType)
        {
            if (FactoryType.Equals("Vegetables"))
                return new VegetablesFactory();
            else
                return new MilkFactory();
        }
    }
}