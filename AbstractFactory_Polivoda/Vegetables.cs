﻿namespace AbstractFactoryDesignPattern
{
    public class VegetablesFactory : ProductFactory
    {
        public override Product GetProduct(string AnimalType)
        {
            if (AnimalType.Equals("Potato"))
            {
                return new Potato();
            }
            else if (AnimalType.Equals("Tomato"))
            {
                return new Tomato();
            }
            else
                return null;
        }
    }
}