﻿namespace AbstractFactoryDesignPattern
{
    public class MilkFactory : ProductFactory
    {
        public override Product GetProduct(string AnimalType)
        {
            if (AnimalType.Equals("Cheese"))
            {
                return new Cheese();
            }
            else if (AnimalType.Equals("Milk"))
            {
                return new Milk();
            }
            else if (AnimalType.Equals("Eggs"))
            {
                return new Eggs();
            }
            else
                return null;
        }
    }
}