﻿namespace AbstractFactoryDesignPattern
{
    public interface Product
    {
        string price();
    }
}