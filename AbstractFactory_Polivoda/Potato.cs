﻿namespace AbstractFactoryDesignPattern
{
    public class Potato : Product
    {
        public string price()
        {
            return "40 grn";
        }
    }
}