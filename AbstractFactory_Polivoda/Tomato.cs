﻿namespace AbstractFactoryDesignPattern
{
    public class Tomato : Product
    {
        public string price()
        {
            return "75 grn";
        }
    }
}