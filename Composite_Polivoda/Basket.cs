﻿using System;

class Basket
{
    static void Main(string[] args)
    {
        var gum = new Leaf("Bubble gum", 10);
        gum.CalculateTotalPrice();
        Console.WriteLine();

        
        var Basket = new CompositeProduct("Meat", 0);
        var Chicken = new Leaf("Chicken", 100);
        var Beaf = new Leaf("Beaf", 120);
        Basket.Add(Chicken);
        Basket.Add(Beaf);


        var Cereals = new CompositeProduct("Cereals", 0);
        var Pasta = new Leaf("Pasta", 40);
        var Rice = new Leaf("Rice", 30);
        Cereals.Add(Pasta);
        Cereals.Add(Rice);
        Basket.Add(Cereals);

        var Drinks = new CompositeProduct("Drinks", 0);
        var Milk = new Leaf("Milk", 25);
        var Soda = new Leaf("Soda", 20);
        Drinks.Add(Milk);
        Drinks.Add(Soda);
        Basket.Add(Drinks);


        Console.WriteLine($"Basket price: {Basket.CalculateTotalPrice()}");
    }
}