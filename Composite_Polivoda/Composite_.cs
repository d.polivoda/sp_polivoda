﻿using System;
using System.Collections.Generic;

public class CompositeProduct : ProductBase, MoveProduct
{
    private List<ProductBase> Product;

    public CompositeProduct(string name, int price)
        : base(name, price)
    {
        Product = new List<ProductBase>();
    }

    public void Add(ProductBase product)
    {
        Product.Add(product);
    }

    public void Remove(ProductBase product)
    {
        Product.Remove(product);
    }

    public override int CalculateTotalPrice()
    {
        int total = 0;

        Console.WriteLine($"{name} included:");

        foreach (var product in Product)
        {
            total += product.CalculateTotalPrice();
        }

        return total;
    }
}