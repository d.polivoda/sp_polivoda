﻿public interface MoveProduct
{
    void Add(ProductBase product);
    void Remove(ProductBase product);
}
