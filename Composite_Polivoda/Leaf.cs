﻿using System;

public class Leaf : ProductBase
{
    public Leaf(string name, int price)
        : base(name, price)
    {
    }

    public override int CalculateTotalPrice()
    {
        Console.WriteLine($"{name}, price: {price}");

        return price;
    }
}