﻿using System.Collections.Generic;
using System.Linq;

public abstract class OrderCommand
{
    public abstract void Execute(List<Product> order, Product newItem);
}
public class NewOrderCommand : OrderCommand
{
    public override void Execute(List<Product> order, Product newItem)
    {
        order.Add(newItem);
    }
}

public class RemoveOrderCommand : OrderCommand
{
    public override void Execute(List<Product> order, Product newItem)
    {
        order.Remove(order.Where(x => x.Name == newItem.Name).First());
    }
}

public class ModifyOrderCommand : OrderCommand
{
    public override void Execute(List<Product> order, Product newItem)
    {
        var item = order.Where(x => x.Name == newItem.Name).First();
        item.Quantity = newItem.Quantity;
        item.Price = newItem.Price;
    }
}