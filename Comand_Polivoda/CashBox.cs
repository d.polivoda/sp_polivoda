﻿using System.Collections.Generic;

public class CashBox
{
    public List<Product> Orders { get; set; }

    public CashBox()
    {
        Orders = new List<Product>();
    }

    public void ExecuteCommand(OrderCommand command, Product item)
    {
        command.Execute(this.Orders, item);
    }

    public void ShowOrders()
    {
        foreach (var item in Orders)
        {
            item.DisplayOrder();
        }
    }
}