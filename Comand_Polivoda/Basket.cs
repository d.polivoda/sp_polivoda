﻿using System;
using System.Collections.Generic;
using System.Text;

public enum CommandType {Add,Modify,Remove}
public class Basket
{
    private CashBox order;

    private CommandFactory commandFactory;

    public Basket()
    {
        order = new CashBox();
        commandFactory = new CommandFactory();
    }

    public void ExecuteCommand(CommandType commandType, Product menuItem)
    {
        OrderCommand orderCommand = commandFactory.GetCommand(commandType);
        order.ExecuteCommand(orderCommand, menuItem);
    }

    public void ShowCurrentOrder()
    {
        order.ShowOrders();
    }
}

public class CommandFactory
{
    //Dine table method
    public OrderCommand GetCommand(CommandType commandType)
    {
        switch (commandType)
        {
            case CommandType.Add:
                return new NewOrderCommand();
            case CommandType.Modify:
                return new ModifyOrderCommand();
            case CommandType.Remove:
                return new RemoveOrderCommand();
            default:
                return new NewOrderCommand();
        }
    }
}
