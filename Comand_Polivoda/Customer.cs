﻿using System;
using System.Collections.Generic;

public class Product
{
    public string Name { get; set; }
    public int Quantity { get; set; }
    public string Price { get; set; }
    

    public void DisplayOrder()
    {
        Console.WriteLine("Product: " + Name);
        Console.WriteLine("Price: " + Price);
        Console.WriteLine("Quantity: " + Quantity);
      
    }
}