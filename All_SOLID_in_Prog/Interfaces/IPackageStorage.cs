﻿using System.Collections.Generic;

namespace all_solids_in_one_prog
{
    public interface IPackageStorage
    {
        IPackage[] GetPackagesFor(string name);
        void Remove(IPackage package);
    }
}
