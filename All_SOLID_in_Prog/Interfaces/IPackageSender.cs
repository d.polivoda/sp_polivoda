﻿namespace all_solids_in_one_prog
{
    public interface IPackageSender
    {
        void Add(IPackage package);
    }
}
