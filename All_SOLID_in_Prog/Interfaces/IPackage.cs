﻿namespace all_solids_in_one_prog
{
    public interface IPackage
    {
        string ReceiverName { get; }

        void ShowInfo();
    }
}
