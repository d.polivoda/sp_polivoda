﻿using System;

namespace all_solids_in_one_prog
{
    public class Box : Package
    {
        public int weight;

        protected override string PackageType => "Box";

        public Box(string senderName, string receiverName, int weight) 
            : base(senderName, receiverName)
        {
            this.weight = weight;
        }

        public override void ShowInfo()
        {
            base.ShowInfo();
            Console.WriteLine($"Weight: {weight}");
        }
    }
}
