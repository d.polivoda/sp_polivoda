﻿using System.Collections.Generic;
using System.Linq;

namespace all_solids_in_one_prog
{
    // Solid The Interface Segregation Principle
    public class PackageStorage : IPackageStorage, IPackageSender
    {
        private List<IPackage> packages = new List<IPackage>();

        public void Add(IPackage package)
        {
            packages.Add(package);
        }

        public IPackage[] GetPackagesFor(string name)
        {
            return packages.Where(package => package.ReceiverName == name).ToArray();
        }

        public void Remove(IPackage package)
        {
            packages.Remove(package);
        }
    }
}
