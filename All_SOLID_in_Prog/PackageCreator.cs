﻿using System;

namespace all_solids_in_one_prog
    
{
    //Solid The Single Responsibility Principle
    public class PackageCreator 
    {
        private readonly IPackageSender sender;

        public PackageCreator(IPackageSender sender)
        {
            this.sender = sender;
        }

        public void CreatePackage()
        {
            Console.WriteLine("Write your name:");
            string senderName = Console.ReadLine();
            Console.WriteLine("Write receiver name:");
            string receiverName = Console.ReadLine();
            Console.WriteLine("Select type of package:");
            Console.WriteLine("1. Letter");
            Console.WriteLine("2. Box");

            string action = Console.ReadLine();
            if (action == "1")
                CreateLetter(senderName, receiverName);
            else if (action == "2")
                CreateBox(senderName, receiverName);
        }

        private void CreateLetter(string senderName, string receiverName)
        {
            Console.WriteLine("Enter letter's content:");
            string content = Console.ReadLine();
            Letter letter = new Letter(senderName, receiverName, content);
            sender.Add(letter);
        }

        private void CreateBox(string senderName, string receiverName)
        {
            Console.WriteLine("Enter box's weight:");
            int weight;
            while (true)
            {
                if (int.TryParse(Console.ReadLine(), out weight))
                    break;
            }
            Box box = new Box(senderName, receiverName, weight);
            sender.Add(box);
        }
    }
}
