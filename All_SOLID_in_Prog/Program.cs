﻿using System;

namespace all_solids_in_one_prog
{
    class Program
    {
        static void Main(string[] args)
        {
            PackageStorage packageStorage = new PackageStorage();
            PackageReceiver packageReceiver = new PackageReceiver(packageStorage);
            PackageCreator packageCreator = new PackageCreator(packageStorage);
            while (true)
            {

                Console.WriteLine("Select Action:");
                Console.WriteLine("1. Receive package");
                Console.WriteLine("2. Send package");

                string action = Console.ReadLine();
                if (action == "1")
                    packageReceiver.Receive();
                else if (action == "2")
                    packageCreator.CreatePackage();
            }
        }
    }
}
