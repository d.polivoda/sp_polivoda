﻿using System;

namespace all_solids_in_one_prog
{
    // Solid The Liskov Substitution Principle
    public class Letter : Package
    {
        public string content;

        protected override string PackageType => "Letter";

        public Letter(string senderName, string receiverName, string content) 
            : base(senderName, receiverName)
        {
            this.content = content;
        }

        public override void ShowInfo()
        {
            base.ShowInfo();
            Console.WriteLine($"Content: {content}");
        }

    }
}
