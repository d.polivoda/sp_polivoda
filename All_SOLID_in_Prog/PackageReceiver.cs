﻿using System;

namespace all_solids_in_one_prog
{
    public class PackageReceiver
    {
        // Solid The Dependency Inversion Principle
        private readonly IPackageStorage storage;

        public PackageReceiver(IPackageStorage storage)
        {
            this.storage = storage;
        }

        public void Receive()
        {
            Console.WriteLine("Enter your name:");
            string name = Console.ReadLine();
            IPackage[] packages = storage.GetPackagesFor(name);
            if (packages != null && packages.Length > 0)
                ReceivePackages(packages);
            else
                Console.WriteLine("There is no packages for you.");
        }

        private void ReceivePackages(IPackage[] packages)
        {
            foreach(IPackage package in packages)
            {
                package.ShowInfo();
                storage.Remove(package);
            }
        }
    }
}
