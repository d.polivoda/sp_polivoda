﻿using System;

namespace all_solids_in_one_prog
{
    // Solid The Open Closed Principle
    public abstract class Package : IPackage
    {
        public string senderName;
        public string ReceiverName { get; }
        protected abstract string PackageType { get; }

        public Package(string senderName, string receiverName)
        {
            this.senderName = senderName;
            ReceiverName = receiverName;
        }

        public virtual void ShowInfo()
        {
            Console.WriteLine($"Package Type: {PackageType}");
            Console.WriteLine($"From: {senderName}");
        }
    }
}
