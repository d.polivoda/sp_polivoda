﻿public class Basket
{
    public string Name { get; set; }
    public string Price { get; set; }
    public int Barcode { get; set; }
}