﻿using System.Collections.Generic;

public static class BasketDataProvider
{
    public static List<Basket> GetData() =>
       new List<Basket>
       {
            new Basket { Price = "50 grn" , Name = "Tomato", Barcode = 959775 },
            new Basket { Price = "25 grn", Name = "Pasta", Barcode = 715586 },
            new Basket { Price = "120 grn", Name = "Chicken meat", Barcode = 154631 },
            new Basket { Price = "40 grn", Name = "Juice", Barcode = 531489 },
            new Basket { Price = "200 grn", Name = "Cheese", Barcode = 541236 }
       };
}