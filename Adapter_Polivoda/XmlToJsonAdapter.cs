﻿using System;
using System.Linq;

public interface IXmlToJson
{
    void ConvertXmlToJson();
}
public class XmlToJsonAdapter : IXmlToJson
{
    private readonly XmlConverter _xmlConverter;

    public XmlToJsonAdapter(XmlConverter xmlConverter)
    {
        _xmlConverter = xmlConverter;
    }

    public void ConvertXmlToJson()
    {
        var basket = _xmlConverter.GetXML()
                .Element("Basket")
                .Elements("Basket")
                .Select(m => new Basket
                {
                    Price = m.Attribute("Price").Value,
                    Name = m.Attribute("Name").Value,
                    Barcode = Convert.ToInt32(m.Attribute("Barcode").Value)
                });

        new JsonConverter(basket)
            .ConvertToJson();
    }
}