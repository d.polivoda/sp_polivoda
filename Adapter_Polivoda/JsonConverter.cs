﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

public class JsonConverter
{
    private IEnumerable<Basket> _basket;

    public JsonConverter(IEnumerable<Basket> basket)
    {
        _basket = basket;
    }

    public void ConvertToJson()
    {
        var jsonbasket = JsonConvert.SerializeObject(_basket, Formatting.Indented);

        Console.WriteLine("\nPrinting JSON list\n");
        Console.WriteLine(jsonbasket);
    }
}