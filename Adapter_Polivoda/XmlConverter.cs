﻿using System;
using System.Linq;
using System.Xml.Linq;

public class XmlConverter
{
    public XDocument GetXML()
    {
        var xDocument = new XDocument();
        var xElement = new XElement("Basket");
        var xAttributes = BasketDataProvider.GetData()
            .Select(m => new XElement("Basket",
                                new XAttribute("Price", m.Price),
                                new XAttribute("Name", m.Name),
                                new XAttribute("Barcode", m.Barcode)));

        xElement.Add(xAttributes);
        xDocument.Add(xElement);

        Console.WriteLine(xDocument);

        return xDocument;
    }
}
